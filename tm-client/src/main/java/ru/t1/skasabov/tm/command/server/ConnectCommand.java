package ru.t1.skasabov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull public static final String NAME = "connect";

    @NotNull private static final String DESCRIPTION = "Connect to server.";

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        @Nullable final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
        getServiceLocator().getSystemEndpoint().setSocket(socket);
    }

}
