package ru.t1.skasabov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.DataYamlSaveFasterXmlRequest;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull private static final String NAME = "data-save-yaml";

    @NotNull private static final String DESCRIPTION = "Save data to yaml file.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest();
        getDomainEndpoint().saveDataYamlFasterXml(request);
    }

}
