package ru.t1.skasabov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

import java.net.Socket;

public interface ISystemEndpoint {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

    @NotNull
    ApplicationSystemInfoResponse getSystemInfo(@NotNull ApplicationSystemInfoRequest request);

    void setSocket(@Nullable Socket socket);

}
