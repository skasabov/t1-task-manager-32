package ru.t1.skasabov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
