package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.request.TaskListRequest;
import ru.t1.skasabov.tm.dto.response.TaskListResponse;
import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-list";

    @NotNull private static final String DESCRIPTION = "Show list tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(sort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTasks(request);
        renderTasks(response.getTasks());
    }

}
