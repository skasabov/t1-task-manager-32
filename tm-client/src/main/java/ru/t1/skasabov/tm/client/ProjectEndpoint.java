package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final AbstractEndpoint client) {
        super(client);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProjects(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectListResponse listProjects(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startProjectById(@NotNull final ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeProjectById(@NotNull final ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final AuthEndpoint authEndpointClient = new AuthEndpoint();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        @NotNull final ProjectEndpoint projectClient = new ProjectEndpoint(authEndpointClient);
        System.out.println(projectClient.createProject(new ProjectCreateRequest("HELLO", "WORLD")));
        System.out.println(projectClient.listProjects(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
