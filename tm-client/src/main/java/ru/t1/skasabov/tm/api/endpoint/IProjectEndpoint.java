package ru.t1.skasabov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

import java.net.Socket;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProjects(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    ProjectListResponse listProjects(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeProjectById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull ProjectCompleteByIndexRequest request);

    void setSocket(@Nullable Socket socket);

}
