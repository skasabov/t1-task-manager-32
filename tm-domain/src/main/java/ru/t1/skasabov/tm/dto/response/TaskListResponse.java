package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @NotNull private List<Task> tasks;

    public TaskListResponse(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

}
