package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIndexRequest extends AbstractUserRequest {

    @NotNull private Integer index;

    public ProjectGetByIndexRequest(@NotNull final Integer index) {
        this.index = index;
    }

}
