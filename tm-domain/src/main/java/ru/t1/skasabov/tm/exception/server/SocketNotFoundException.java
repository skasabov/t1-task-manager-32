package ru.t1.skasabov.tm.exception.server;

import ru.t1.skasabov.tm.exception.AbstractException;

public final class SocketNotFoundException extends AbstractException {

    public SocketNotFoundException() {
        super("Error! Socket connection is not found...");
    }

}
