package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ApplicationSystemInfoResponse extends AbstractResponse {

    @NotNull private Integer availableProcessors;

    @NotNull private String freeMemory;

    @NotNull private String maximumMemory;

    @NotNull private String totalMemory;

    @NotNull private String usageMemory;

}
