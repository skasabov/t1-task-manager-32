package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @NotNull
    @Override
    public ApplicationSystemInfoResponse getSystemInfo(@NotNull final ApplicationSystemInfoRequest request) {
        return call(request, ApplicationSystemInfoResponse.class);
    }

    public static void main(@Nullable final String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ApplicationAboutResponse applicationAboutResponse = client.getAbout(
                new ApplicationAboutRequest()
        );
        System.out.println(applicationAboutResponse.getEmail());
        System.out.println(applicationAboutResponse.getName());

        @NotNull final ApplicationVersionResponse applicationVersionResponse = client.getVersion(
                new ApplicationVersionRequest()
        );
        System.out.println(applicationVersionResponse.getVersion());

        @NotNull final ApplicationSystemInfoResponse response = client.getSystemInfo(
                new ApplicationSystemInfoRequest()
        );
        System.out.println(response.getAvailableProcessors());
        System.out.println(response.getFreeMemory());
        System.out.println(response.getMaximumMemory());
        System.out.println(response.getTotalMemory());
        System.out.println(response.getUsageMemory());

        client.disconnect();
    }

}
