package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.skasabov.tm.api.service.IProjectTaskService;
import ru.t1.skasabov.tm.api.service.IServiceLocator;
import ru.t1.skasabov.tm.api.service.ITaskService;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public TaskGetByProjectIdResponse findAllByProjectId(
            @NotNull final TaskGetByProjectIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskGetByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(
            @NotNull final TaskBindToProjectRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTasks(@NotNull final TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().removeAll(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final TaskSort sort = request.getSort();
        final boolean checkSort = sort == null;
        @Nullable final Comparator<Task> comparator = (checkSort ? null : sort.getComparator());
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, comparator);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().removeOneById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().removeOneByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

}
