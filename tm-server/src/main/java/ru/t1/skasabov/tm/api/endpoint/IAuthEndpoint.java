package ru.t1.skasabov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}
